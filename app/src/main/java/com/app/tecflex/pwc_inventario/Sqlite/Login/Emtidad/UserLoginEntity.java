package com.app.tecflex.pwc_inventario.Sqlite.Login.Emtidad;

public class UserLoginEntity {
    private String codigo;
    private String descripcion;
    private String password;

    public UserLoginEntity(String codigo, String descripcion, String password) {
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.password = password;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
