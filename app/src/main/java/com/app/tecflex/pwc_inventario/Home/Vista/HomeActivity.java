package com.app.tecflex.pwc_inventario.Home.Vista;

import androidx.annotation.ColorInt;
import androidx.annotation.ColorRes;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;

import com.app.tecflex.pwc_inventario.Home.Adapter.DrawerAdapter;
import com.app.tecflex.pwc_inventario.Home.Entidad.DrawerItem;
import com.app.tecflex.pwc_inventario.Home.Entidad.SimpleItem;
import com.app.tecflex.pwc_inventario.Home.Entidad.SpaceItem;
import com.app.tecflex.pwc_inventario.Home.Fragments.Configuracion.Vista.ConfiguracionFragment;
import com.app.tecflex.pwc_inventario.Home.Fragments.Consulta.Vista.ConsultaFragment;
import com.app.tecflex.pwc_inventario.Home.Fragments.Inventario.Vista.InventarioFragment;
import com.app.tecflex.pwc_inventario.Home.Fragments.Inventario.Vista.InventarioFragment2;
import com.app.tecflex.pwc_inventario.Login.Vista.MainActivity;
import com.app.tecflex.pwc_inventario.R;
import com.app.tecflex.pwc_inventario.Utilitarios.Global;
import com.app.tecflex.pwc_inventario.databinding.ActivityHomeBinding;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.yarolegovich.slidingrootnav.SlidingRootNav;
import com.yarolegovich.slidingrootnav.SlidingRootNavBuilder;

import java.util.Arrays;

public class HomeActivity extends AppCompatActivity implements DrawerAdapter.OnItemSelectedListener {

    private ActivityHomeBinding binding;

    private static final int POS_CLOSE = 0;
    private static final int POS_INVENTARIO = 1;
    private static final int POS_CONFIGURACION = 2;
    private static final int POS_CONSULTA = 3;
    private static final int POS_LOGOUT = 5;

    private String[] screenTitles;
    private Drawable[] screenIcons;

    private SlidingRootNav slidingRootNav;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityHomeBinding.inflate(getLayoutInflater());
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(binding.getRoot());

        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setTitle("Inventario Act. Fijo");
        binding.toolbar.setTitleTextColor(getResources().getColor(R.color.grisTitle));
        //getSupportActionBar().setLogo(R.drawable.price_logo2);
        //getSupportActionBar().setDisplayShowCustomEnabled(true);
        //getSupportActionBar().setCustomView(R.layout.tool_bar_custom);

        slidingRootNav = new SlidingRootNavBuilder(this)
                .withDragDistance(180)
                .withRootViewScale(0.75f)
                .withRootViewElevation(25)
                .withToolbarMenuToggle(binding.toolbar)
                .withMenuOpened(false)
                .withContentClickableWhenMenuOpened(false)
                .withSavedState(savedInstanceState)
                .withMenuLayout(R.layout.drawer_menu)
                .inject();

        screenIcons = loadScreenIcons();

        screenTitles = loadScreenTitles();

        DrawerAdapter adapter = new DrawerAdapter(Arrays.asList(
                createItemFor(POS_CLOSE),
                createItemFor(POS_INVENTARIO).setChecked(true),
                createItemFor(POS_CONFIGURACION),
                createItemFor(POS_CONSULTA),
                new SpaceItem(260),//260
                createItemFor(POS_LOGOUT)
        ));
        adapter.setListener(this);

        RecyclerView list = findViewById(R.id.drawer_list);
        list.setNestedScrollingEnabled(false);
        list.setLayoutManager(new LinearLayoutManager(this));
        list.setAdapter(adapter);

        adapter.setSelected(POS_INVENTARIO);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.user){//\n
            MensajeDialog("Inventariador", Global.codInventariador+"\n"+Global.descInventariador);
        }else if(id == R.id.logOut){
            MensajeDialogSesion("Inventario","¿Seguro que desea cerrar sesión?");
        }else if(id == R.id.terminal){
            MensajeDialog("Terminal", "Indicar número de terminal");
        }

        return true;
    }

    private DrawerItem createItemFor(int position){
        return new SimpleItem(screenIcons[position],screenTitles[position])
                .withIconTint(color(R.color.white2))
                .withTextTint(color(R.color.white2))
                .withSelectedIconTint(color(R.color.amarillo1))
                .withSelectedTextTint(color(R.color.amarillo1));
    }

    @ColorInt
    private int color(@ColorRes int res){
        return ContextCompat.getColor(this,res);
    }

    private String[] loadScreenTitles() {
        return getResources().getStringArray(R.array.id_activityScreenTitles);
    }

    private Drawable[] loadScreenIcons() {
        TypedArray ta = getResources().obtainTypedArray(R.array.id_activityScreenIcons);
        Drawable[]  icons = new Drawable[ta.length()];
        for(int i = 0; i < ta.length(); i++){
            int id = ta.getResourceId(i,0);
            if(id != 0){
                icons[i] = ContextCompat.getDrawable(this,id);
            }
        }
        ta.recycle();
        return icons;
    }

    @Override
    public void onItemSelected(int position) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if(position == POS_INVENTARIO){
            getSupportActionBar().setTitle("Inventario Act. Fijo");
            InventarioFragment inventarioFragment = new InventarioFragment();
            transaction.replace(binding.container.getId(),inventarioFragment);
            //InventarioFragment2 inventarioFragment = new InventarioFragment2();
            //transaction.replace(binding.container.getId(),inventarioFragment);

        } else if(position == POS_CONFIGURACION){
            getSupportActionBar().setTitle("Configuración");
            ConfiguracionFragment configuracionFragment = new ConfiguracionFragment();
            transaction.replace(binding.container.getId(),configuracionFragment);

        } else if(position == POS_CONSULTA){
            getSupportActionBar().setTitle("Consulta");
            ConsultaFragment consultaFragment = new ConsultaFragment();
            transaction.replace(binding.container.getId(),consultaFragment);

        } else if (position == POS_LOGOUT){

            MensajeDialogSesion("Inventario","¿Seguro que desea cerrar sesión?");
        }

        slidingRootNav.closeMenu();
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void MensajeDialogSesion(String Title, String Mensaje) {
        new MaterialAlertDialogBuilder(HomeActivity.this, R.style.materialAlertCustom)
                .setTitle(Title)
                .setMessage(Mensaje)
                .setCancelable(false)
                .setPositiveButton("Sí", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(HomeActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    private void MensajeDialog(String Title, String Mensaje) {
        new MaterialAlertDialogBuilder(HomeActivity.this, R.style.materialAlertCustom)
                .setTitle(Title)
                .setMessage(Mensaje)
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }
}