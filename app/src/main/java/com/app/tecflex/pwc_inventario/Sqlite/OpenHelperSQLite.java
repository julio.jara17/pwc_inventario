package com.app.tecflex.pwc_inventario.Sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class OpenHelperSQLite extends SQLiteOpenHelper {

    public static final String DB_NAME = "BDPWC";
    private static final int DB_VERSION = 3;

    public OpenHelperSQLite(Context context) {

        //super(context, DB_NAME, null, DB_VERSION);
        super(new DatabaseContext(context),DB_NAME,null,DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        /*db.execSQL("create table inventariadores (id INTEGER PRIMARY KEY AUTOINCREMENT ,codigo TEXT ,descripcion TEXT NOT NULL,password TEXT NOT NULL)");
        db.execSQL("CREATE TABLE Local (id INTEGER PRIMARY KEY AUTOINCREMENT,codigo TEXT,descripcion TEXT)");
        db.execSQL("CREATE TABLE Piso (id INTEGER NOT NULL,codigo TEXT, descripcion TEXT, codigoLocal TEXT ,FOREIGN KEY(id) REFERENCES Local (id))");
        db.execSQL("CREATE TABLE Zona (id INTEGER PRIMARY KEY AUTOINCREMENT, codigo TEXT,descripcion TEXT);");
        db.execSQL("CREATE TABLE Area (id INTEGER PRIMARY KEY AUTOINCREMENT, codigo TEXT,descripcion TEXT);");
        db.execSQL("CREATE TABLE CentroCosto (id INTEGER PRIMARY KEY AUTOINCREMENT,codigo TEXT,descripcion TEXT, FOREIGN KEY(id) REFERENCES Usuario(id));");
        db.execSQL("CREATE TABLE Usuario(id INTEGER PRIMARY KEY AUTOINCREMENT,codigo TEXT , descripcion TEXT, costo TEXT);");

        db.execSQL("create table tab_inventario (id INTEGER PRIMARY KEY AUTOINCREMENT ,cod_activo TEXT ,cod_catalogo TEXT ,cod_local TEXT, cod_zona TEXT, cod_area TEXT, cod_usuario TEXT, " +
                "cod_estado TEXT, cod_uso TEXT, cod_asociacion TEXT, cod_propiedad TEXT, cod_color TEXT, cod_material TEXT, marca TEXT, modelo TEXT, serie TEXT, largo TEXT, " +
                "ancho TEXT, alto TEXT, observacion TEXT, descripcion TEXT, descripAnte1 TEXT, descripAnte2 TEXT, etiquetaNueva TEXT, campAdicional1 TEXT, campAdicional2 TEXT, " +
                "campAdicional3 TEXT, campAdicional4 TEXT, campAdicional5 TEXT, cod_inventariador TEXT, fecha TEXT, hora TEXT, pda TEXT, flag TEXT)");*/
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        /*db.execSQL("DROP TABLE IF EXISTS inventariadores");
        db.execSQL("CREATE TABLE inventariadores(id INTEGER PRIMARY KEY AUTOINCREMENT ,codigo TEXT ,descripcion TEXT NOT NULL,password TEXT NOT NULL)");
        db.execSQL("DROP TABLE IF EXISTS Local");
        db.execSQL("CREATE TABLE Local (id INTEGER PRIMARY KEY AUTOINCREMENT,codigo TEXT,descripcion TEXT)");
        db.execSQL("DROP TABLE IF EXISTS Piso");
        db.execSQL("CREATE TABLE Piso (id INTEGER NOT NULL,codigo TEXT, descripcion TEXT, codigoLocal TEXT ,FOREIGN KEY(id) REFERENCES Local (id))");
        db.execSQL("DROP TABLE IF EXISTS Zona");
        db.execSQL("CREATE TABLE Zona (id INTEGER PRIMARY KEY AUTOINCREMENT, codigo TEXT,descripcion TEXT);");
        db.execSQL("DROP TABLE IF EXISTS Area");
        db.execSQL("CREATE TABLE Area (id INTEGER PRIMARY KEY AUTOINCREMENT, codigo TEXT,descripcion TEXT);");
        db.execSQL("DROP TABLE IF EXISTS CentroCosto");
        db.execSQL("CREATE TABLE CentroCosto (id INTEGER PRIMARY KEY AUTOINCREMENT,codigo TEXT,descripcion TEXT, FOREIGN KEY(id) REFERENCES Usuario(id));");
        db.execSQL("DROP TABLE IF EXISTS Usuario");
        db.execSQL("CREATE TABLE Usuario(id INTEGER PRIMARY KEY AUTOINCREMENT,codigo TEXT NOT NULL, descripcion TEXT, costo TEXT);");

        db.execSQL("DROP TABLE IF EXISTS tab_inventario");
        db.execSQL("create table tab_inventario (id INTEGER PRIMARY KEY AUTOINCREMENT ,cod_activo TEXT ,cod_catalogo TEXT ,cod_local TEXT, cod_zona TEXT, cod_area TEXT, cod_usuario TEXT, " +
                "cod_estado TEXT, cod_uso TEXT, cod_asociacion TEXT, cod_propiedad TEXT, cod_color TEXT, cod_material TEXT, marca TEXT, modelo TEXT, serie TEXT, largo TEXT, " +
                "ancho TEXT, alto TEXT, observacion TEXT, descripcion TEXT, descripAnte1 TEXT, descripAnte2 TEXT, etiquetaNueva TEXT, campAdicional1 TEXT, campAdicional2 TEXT, " +
                "campAdicional3 TEXT, campAdicional4 TEXT, campAdicional5 TEXT, cod_inventariador TEXT, fecha TEXT, hora TEXT, pda TEXT, flag TEXT)");*/
    }
}
