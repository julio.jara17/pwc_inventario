package com.app.tecflex.pwc_inventario.Home.Fragments.Inventario.Interfaces;

import android.content.Context;

public interface Inventario2Interfaces {
    interface Vista{
        void inventariarVista(String codigo, Context context);

        void mostrarCabecera();
    }

    interface Presenter{
        void inventariarPresenter(String codigo, Context context);
    }

    interface Interactor{
        void inventariarInteractor(String codigo, Context context);
    }
}
