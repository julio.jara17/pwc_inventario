package com.app.tecflex.pwc_inventario.Home.Fragments.Inventario.Interactor;

import android.content.Context;

import com.app.tecflex.pwc_inventario.Home.Fragments.Inventario.Interfaces.Inventario2Interfaces;
import com.app.tecflex.pwc_inventario.Sqlite.Inventario.InventarioDAO;

public class Inventario2Interactor implements Inventario2Interfaces.Interactor {

    private Inventario2Interfaces.Presenter presenter;
    InventarioDAO inventarioDAO;

    public Inventario2Interactor(Inventario2Interfaces.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void inventariarInteractor(String codigo, Context context) {
        inventarioDAO = new InventarioDAO(context);
    }
}
