package com.app.tecflex.pwc_inventario.Login.Interfaces;

import android.app.Activity;
import android.content.Context;

import com.app.tecflex.pwc_inventario.Sqlite.Login.Emtidad.UserLoginEntity;

import java.util.ArrayList;

public interface LoginInterfaces {

    interface Vista{
        void validarUsuarioVista(String user, String password, Context context);

        void mostrarRespuestaLoginVista(ArrayList<UserLoginEntity> respuestaLogin);

        void consultaListaUsuariosVista(Context context);

        void mostrarListaUsuariosVista(ArrayList<String> usuarios);

        void MensajeSalir(String Title, String Mensaje);

        void MensajeDialog(String Title, String Mensaje);

        void mostrarTeclado(Activity activity);

        void ocultarTeclado(Activity activity);

        void settearDescUsuario(String codigo);
    }

    interface Presenter{
        void consultaListaUsuariosPresenter(Context context);

        void mostrarListaUsuarioPresenter(ArrayList<String> usuarios);

        void validarUsuarioPresenter(String user, String password, Context context);

        void mostrarRespuestaLoginPresenter(ArrayList<UserLoginEntity> respuestaLogin);
    }

    interface Interactor{
        void consultaListaUsuariosInteractor(Context context);

        void mostrarListaUsuariosInteractor(ArrayList<String> usuarios);

        void validarUsuarioInteractor(String user, String password, Context context);

        void mostrarRespuestaLoginInteractor(ArrayList<UserLoginEntity> respuestaLogin);
    }
}
