package com.app.tecflex.pwc_inventario.Sqlite.Login;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.app.tecflex.pwc_inventario.Sqlite.Login.Emtidad.UserLoginEntity;
import com.app.tecflex.pwc_inventario.Sqlite.OpenHelperSQLite;

import java.util.ArrayList;

public class LoginDAO {

    private Context context;
    private String tabla="inventariadores";

    public LoginDAO(Context context) {
        this.context = context;
    }

    public ArrayList<UserLoginEntity> ValidarUsuarioDAO(String user, String password) {
        ArrayList<UserLoginEntity> userResponse = null;
        OpenHelperSQLite openHelper = new OpenHelperSQLite(context);
        SQLiteDatabase bd = openHelper.getReadableDatabase();
        try {
            Cursor c;
            int cantReg = bd.rawQuery("SELECT codigo,descripcion,password  FROM " + tabla, null).getCount();
            if (cantReg > 0) {
                c = bd.rawQuery("SELECT codigo,descripcion,password  FROM " + tabla + " WHERE descripcion = '" + user + "' AND password = '" + password + "'", null);
                if (c.moveToFirst()) {
                    userResponse = new ArrayList<>();
                    do {
                        UserLoginEntity userLoginEntity = new UserLoginEntity(
                                c.getString(0),
                                c.getString(1),
                                c.getString(2)
                        );
                        userResponse.add(userLoginEntity);
                    } while (c.moveToNext());
                    c.close();
                    openHelper.close();
                    bd.close();
                }
            } else {

            }
        } catch (Exception ex) {
            openHelper.close();
            bd.close();
        }
        return userResponse;
    }

    public ArrayList<String> ListaUsuarios() {
        ArrayList<String> listUser = null;
        OpenHelperSQLite openHelper = new OpenHelperSQLite(context);
        SQLiteDatabase bd = openHelper.getReadableDatabase();
        try {
            Cursor c;
            int cantReg = bd.rawQuery("SELECT codigo FROM inventariadores", null).getCount();
            if (cantReg > 0) {
                c = bd.rawQuery("SELECT codigo,descripcion FROM inventariadores  ORDER BY codigo ", null);
                if (c.moveToFirst()) {
                    listUser = new ArrayList<>();
                    do {
                        String userLoginEntity = new String(
                                c.getString(0)+" - "+c.getString(1)
                        );
                        listUser.add(userLoginEntity);
                    } while (c.moveToNext());
                    c.close();
                    openHelper.close();
                    bd.close();
                } else {

                }
            }
        } catch (Exception ex) {
            openHelper.close();
            bd.close();
        }
        return listUser;
    }

}
