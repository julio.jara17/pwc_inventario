package com.app.tecflex.pwc_inventario.Login.Fragment;

import android.os.Bundle;

import androidx.fragment.app.DialogFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.SearchView;

import com.app.tecflex.pwc_inventario.Login.Vista.MainActivity;
import com.app.tecflex.pwc_inventario.Utilitarios.Global;
import com.app.tecflex.pwc_inventario.databinding.FragmentDialogListUserBinding;


public class DialogListUserFragment extends DialogFragment {

    private static final String TAG = "DialogListUserFragment";
    private FragmentDialogListUserBinding binding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentDialogListUserBinding.inflate(inflater,container,false);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, Global.listaUsuarios);
        binding.lvUsuarios.setAdapter(adapter);

        binding.tvCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });

        binding.lvUsuarios.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Object object = parent.getAdapter().getItem(position);
                String[] DescUsuario = object.toString().split("-");
                //binding.tieUsuario.setText(DescUsuario[0].trim());
                ((MainActivity)getActivity()).settearDescUsuario(DescUsuario[1].trim());
                getDialog().dismiss();
            }
        });

        binding.svUsuario.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return false;
            }
        });

        return binding.getRoot();
    }
}