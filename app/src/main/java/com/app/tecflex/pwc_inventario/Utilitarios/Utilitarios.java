package com.app.tecflex.pwc_inventario.Utilitarios;

import android.annotation.SuppressLint;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class Utilitarios {

    public static String getCurrentDate() {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setTimeZone(TimeZone.getTimeZone(Global.DATE_TIME_ZONE));
        Date today = Calendar.getInstance().getTime();
        return dateFormat.format(today);
    }

}
