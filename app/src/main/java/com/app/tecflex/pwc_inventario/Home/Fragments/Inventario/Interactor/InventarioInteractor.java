package com.app.tecflex.pwc_inventario.Home.Fragments.Inventario.Interactor;

import android.content.Context;

import com.app.tecflex.pwc_inventario.Home.Fragments.Inventario.Interfaces.InventarioInterfaces;
import com.app.tecflex.pwc_inventario.Sqlite.Inventario.InventarioDAO;

import java.util.ArrayList;

public class InventarioInteractor implements InventarioInterfaces.Interactor {

    private InventarioInterfaces.Presenter presenter;
    InventarioDAO inventarioDAO;

    public InventarioInteractor(InventarioInterfaces.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void consultaListadoDataInteractor(Context context, int ident) {
        inventarioDAO = new InventarioDAO(context);
        ArrayList<String> listadoLocal = inventarioDAO.ListadoData(ident);
        mostrarListadoDataInteractor(listadoLocal, ident);
    }

    @Override
    public void mostrarListadoDataInteractor(ArrayList<String> listadoLocal, int ident) {
        presenter.mostrarListadoDataPresenter(listadoLocal, ident);
    }

    @Override
    public void consultaPisoListadoInteractor(Context context, String cod) {
        inventarioDAO =new InventarioDAO((context));
        ArrayList<String> listadoPiso = inventarioDAO.ListadoPiso(cod);
        mostrarPisoListadoInteractor(listadoPiso);
    }



    @Override
    public void mostrarPisoListadoInteractor(ArrayList<String> listadoPiso) {
        presenter.mostrarPisoListadoPresenter(listadoPiso);
    }
}
