package com.app.tecflex.pwc_inventario.Login.Presenter;

import android.content.Context;

import com.app.tecflex.pwc_inventario.Login.Interactor.LoginInteractor;
import com.app.tecflex.pwc_inventario.Login.Interfaces.LoginInterfaces;
import com.app.tecflex.pwc_inventario.Sqlite.Login.Emtidad.UserLoginEntity;

import java.util.ArrayList;

public class LoginPresenter implements LoginInterfaces.Presenter {

    private LoginInterfaces.Vista view;
    private LoginInterfaces.Interactor interactor;

    public LoginPresenter(LoginInterfaces.Vista view) {
        this.view = view;
        interactor = new LoginInteractor(this);
    }

    @Override
    public void consultaListaUsuariosPresenter(Context context) {
        interactor.consultaListaUsuariosInteractor(context);
    }

    @Override
    public void mostrarListaUsuarioPresenter(ArrayList<String> usuarios) {
        view.mostrarListaUsuariosVista(usuarios);
    }

    @Override
    public void validarUsuarioPresenter(String user, String password, Context context) {
        interactor.validarUsuarioInteractor(user, password, context);
    }

    @Override
    public void mostrarRespuestaLoginPresenter(ArrayList<UserLoginEntity> respuestaLogin) {
        view.mostrarRespuestaLoginVista(respuestaLogin);
    }

}
