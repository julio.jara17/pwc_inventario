package com.app.tecflex.pwc_inventario.Home.Fragments.Configuracion.Vista;

import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.tecflex.pwc_inventario.R;
import com.app.tecflex.pwc_inventario.databinding.FragmentConfiguracionBinding;

import org.eazegraph.lib.models.PieModel;


public class ConfiguracionFragment extends Fragment {

    private FragmentConfiguracionBinding binding;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentConfiguracionBinding.inflate(inflater,container,false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setDataPieEliminarChart("10","30","70");

        setDataPieCantRegistros("350","650");

        binding.tvNumTerminal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDataPieEliminarChart("10","30","70");
                setDataPieCantRegistros("350","650");
            }
        });
    }

    private void setDataPieEliminarChart(String dat1, String dat2, String dat3) {

        binding.piechart.clearChart();

        binding.piechart.addPieSlice(
                new PieModel(
                        "R",
                        Integer.parseInt(dat1),
                        Color.parseColor("#FFA726")));
        binding.piechart.addPieSlice(
                new PieModel(
                        "Python",
                        Integer.parseInt(dat2),
                        Color.parseColor("#66BB6A")));
        binding.piechart.addPieSlice(
                new PieModel(
                        "C++",
                        Integer.parseInt(dat3),
                        Color.parseColor("#EF5350")));

        binding.piechart.startAnimation();
    }

    private void setDataPieCantRegistros(String dat1, String dat2) {

        binding.piechartRegistros.clearChart();

        binding.piechartRegistros.addPieSlice(
                new PieModel(
                        "Descargados",
                        Integer.parseInt(dat1),
                        Color.parseColor("#FFA726")));
        binding.piechartRegistros.addPieSlice(
                new PieModel(
                        "Sin Descargar",
                        Integer.parseInt(dat2),
                        Color.parseColor("#66BB6A")));

        binding.piechartRegistros.startAnimation();
    }
}