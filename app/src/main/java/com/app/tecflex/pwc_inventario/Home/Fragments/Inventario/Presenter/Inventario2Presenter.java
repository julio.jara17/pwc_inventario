package com.app.tecflex.pwc_inventario.Home.Fragments.Inventario.Presenter;

import android.content.Context;

import com.app.tecflex.pwc_inventario.Home.Fragments.Inventario.Interactor.Inventario2Interactor;
import com.app.tecflex.pwc_inventario.Home.Fragments.Inventario.Interfaces.Inventario2Interfaces;

public class Inventario2Presenter implements Inventario2Interfaces.Presenter {

    private Inventario2Interfaces.Vista view;
    private Inventario2Interfaces.Interactor interactor;

    public Inventario2Presenter(Inventario2Interfaces.Vista view) {
        this.view = view;
        interactor = new Inventario2Interactor(this);
    }

    @Override
    public void inventariarPresenter(String codigo, Context context) {

    }
}
