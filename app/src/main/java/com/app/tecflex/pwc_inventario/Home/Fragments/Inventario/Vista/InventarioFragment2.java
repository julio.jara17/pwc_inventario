package com.app.tecflex.pwc_inventario.Home.Fragments.Inventario.Vista;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.tecflex.pwc_inventario.Home.Fragments.Inventario.Interfaces.Inventario2Interfaces;
import com.app.tecflex.pwc_inventario.Home.Fragments.Inventario.Presenter.Inventario2Presenter;
import com.app.tecflex.pwc_inventario.Home.Vista.HomeActivity;
import com.app.tecflex.pwc_inventario.Login.Vista.MainActivity;
import com.app.tecflex.pwc_inventario.R;
import com.app.tecflex.pwc_inventario.Utilitarios.Global;
import com.app.tecflex.pwc_inventario.databinding.FragmentInventario2Binding;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

public class InventarioFragment2 extends Fragment implements Inventario2Interfaces.Vista {

    private FragmentInventario2Binding binding;
    private Inventario2Interfaces.Presenter presenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentInventario2Binding.inflate(inflater,container,false);
        presenter = new Inventario2Presenter(this);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mostrarCabecera();

        binding.llVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Global.cabeceraInventario = "";
                getFragmentManager().beginTransaction().replace(R.id.container, new InventarioFragment()).commit();
            }
        });

        binding.btnBuscarAlmacen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inventariarVista(binding.etCodigo.getText().toString(),getContext());
            }
        });
    }

    @Override
    public void inventariarVista(String codigo, Context context) {
        presenter.inventariarPresenter(codigo,context);
    }

    @Override
    public void mostrarCabecera() {
        String[] cabecera = Global.cabeceraInventario.split(";");
        binding.tvLocal.setText(cabecera[0].trim());
        binding.tvPiso.setText(cabecera[1].trim());
        binding.tvZona.setText(cabecera[2].trim());
        binding.tvArea.setText(cabecera[3].trim());
        binding.tvCCosto.setText(cabecera[4].trim());
        binding.tvUsuario.setText(cabecera[5].trim());
    }
}