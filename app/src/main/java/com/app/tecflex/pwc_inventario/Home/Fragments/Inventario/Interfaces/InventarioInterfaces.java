package com.app.tecflex.pwc_inventario.Home.Fragments.Inventario.Interfaces;

import android.content.Context;

import java.util.ArrayList;

public interface InventarioInterfaces {

    interface Vista {
        void consultaListadoDataVista(Context context, int ident);

        void consultaPisoListadoVista(Context context, String cod);

        void mostrarListadoDataVista(ArrayList<String> listadoLocal, int ident);

        void mostrarPisoListadoVista(ArrayList<String> istadoPiso);

        void mostrarMaestroFragment();

        void mostrarData(ArrayList<String> lista, int ident);

        void MensajeDialog(String Title, String Mensaje);


        String obtenerCabecera();

        void retornarDatos();

    }

    interface Presenter {
        void consultaListadoPresenter(Context context, int ident);

        void consultaPisoListadoPresenter(Context context, String cod);

        void mostrarListadoDataPresenter(ArrayList<String> listadoLocal, int ident);

        void mostrarPisoListadoPresenter(ArrayList<String> listadoPiso);


    }

    interface Interactor {

        void consultaListadoDataInteractor(Context context, int ident);

        void consultaPisoListadoInteractor(Context context, String cod);

        void mostrarListadoDataInteractor(ArrayList<String> listadoLocal, int ident);

        void mostrarPisoListadoInteractor(ArrayList<String> listadoPiso);


    }
}
