package com.app.tecflex.pwc_inventario.Login.Interactor;

import android.content.Context;

import com.app.tecflex.pwc_inventario.Login.Interfaces.LoginInterfaces;
import com.app.tecflex.pwc_inventario.Sqlite.Login.Emtidad.UserLoginEntity;
import com.app.tecflex.pwc_inventario.Sqlite.Login.LoginDAO;

import java.util.ArrayList;

public class LoginInteractor implements LoginInterfaces.Interactor {

    LoginDAO loginDAO;

    private LoginInterfaces.Presenter presenter;

    public LoginInteractor(LoginInterfaces.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void consultaListaUsuariosInteractor(Context context) {
        loginDAO = new LoginDAO(context);
        ArrayList<String> respuesta= loginDAO.ListaUsuarios();
        mostrarListaUsuariosInteractor(respuesta);
    }

    @Override
    public void mostrarListaUsuariosInteractor(ArrayList<String> usuarios) {
        presenter.mostrarListaUsuarioPresenter(usuarios);
    }

    @Override
    public void validarUsuarioInteractor(String user, String password, Context context) {
        //ConsultarUsuario(user, password, context);
        loginDAO = new LoginDAO(context);
        ArrayList<UserLoginEntity> respuesta =loginDAO.ValidarUsuarioDAO(user, password);
        mostrarRespuestaLoginInteractor(respuesta);
    }

    @Override
    public void mostrarRespuestaLoginInteractor(ArrayList<UserLoginEntity> respuestaLogin) {
        presenter.mostrarRespuestaLoginPresenter(respuestaLogin);
    }
}
