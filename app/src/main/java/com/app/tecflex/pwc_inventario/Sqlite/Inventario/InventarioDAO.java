package com.app.tecflex.pwc_inventario.Sqlite.Inventario;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.app.tecflex.pwc_inventario.Sqlite.OpenHelperSQLite;
import com.app.tecflex.pwc_inventario.Utilitarios.Global;

import java.util.ArrayList;

public class InventarioDAO {
    Context context;
    String NOMBRE_TABLA;

    public InventarioDAO(Context context) {
        this.context = context;
    }

    public ArrayList<String> ListadoData(int ident) {
        ArrayList<String> listData = null;
        OpenHelperSQLite openHelper = new OpenHelperSQLite(context);
        SQLiteDatabase bd = openHelper.getReadableDatabase();
        try {
            Cursor c;
            nombreTabla(ident);
            int cantReg = bd.rawQuery("SELECT codigo FROM " + NOMBRE_TABLA, null).getCount();
            if (cantReg > 0) {
                c = bd.rawQuery("SELECT codigo,descripcion FROM " + NOMBRE_TABLA + " ORDER BY codigo", null);
                if (c.moveToFirst()) {
                    listData = new ArrayList<>();
                    do {
                        String localInventarioEntity = new String(
                                c.getString(0) + " - " + c.getString(1)
                        );
                        listData.add(localInventarioEntity);
                    } while (c.moveToNext());
                    c.close();
                    openHelper.close();
                    bd.close();
                }
            }
        } catch (Exception ex) {
            openHelper.close();
            bd.close();
        }
        return listData;
    }

    public ArrayList<String> ListadoPiso(String cod) {
        ArrayList<String> lisPiso = null;
        OpenHelperSQLite openHelper = new OpenHelperSQLite(context);
        SQLiteDatabase bd = openHelper.getReadableDatabase();
        try {
            Cursor c;
            String[] codigo = cod.split("-");
            int cantReg = bd.rawQuery("Select codigo,descripcion from piso", null).getCount();
            if (cantReg > 0) {
                c = bd.rawQuery("SELECT codigo,descripcion FROM piso WHERE codigoLocal = '" + codigo[0].trim() + "' ORDER BY descripcion;", null);
                if (c.moveToFirst()) {
                    lisPiso = new ArrayList<>();
                    do {
                        String localInventarioEntity = new String(
                                c.getString(0) + " - " + c.getString(1)
                        );
                        lisPiso.add(localInventarioEntity);
                    } while (c.moveToNext());
                    c.close();
                    openHelper.close();
                    bd.close();
                }
            }
        } catch (Exception ex) {
            openHelper.close();
            bd.close();
        }
        return lisPiso;

    }

    public void AgregarData(String codigo, String descripcion) {

        OpenHelperSQLite openHelper = new OpenHelperSQLite(context);
        SQLiteDatabase bd = openHelper.getWritableDatabase();
        nombreTabla(Global.idMaestro);

        if (bd != null) {
            bd.execSQL("INSERT INTO " + NOMBRE_TABLA + " (codigo,descripcion) VALUES ( '" + codigo + "' , '" + descripcion + "' );");
            bd.close();
            openHelper.close();
        } else {
            bd.close();
            openHelper.close();
        }
    }

    public void AgregarPiso(String codigo, String descripcion, String codLocal) {
        OpenHelperSQLite openHelper = new OpenHelperSQLite(context);
        SQLiteDatabase bd = openHelper.getWritableDatabase();
        if (bd != null) {
            bd.execSQL("INSERT INTO Piso (codigo,descripcion,codigoLocal) VALUES ( '" + codigo + "' , '" + descripcion + "' , '" + codLocal + "');");
            bd.close();
            openHelper.close();
        } else {
            bd.close();
            openHelper.close();
        }
    }

    public String mostrarEditextView(int ident) {
        String codData ="";
        String codnum="";
        OpenHelperSQLite openHelper = new OpenHelperSQLite(context);
        SQLiteDatabase bd = openHelper.getReadableDatabase();
        if (bd != null) {
            nombreTabla(ident);
            Cursor c;
            int cantReg = bd.rawQuery("select codigo from " + NOMBRE_TABLA + "  order by rowID desc limit 1", null).getCount();
            if (cantReg > 0) {
                c = bd.rawQuery("select codigo from " + NOMBRE_TABLA + "  order by rowID desc limit 1", null);
                if (c.moveToFirst()) {
                    String Letra ="";
                    for(int i=1;i<=c.getString(0).length();i++){
                        String a = c.getString(0);
                        if(isNumeric(c.getString(0).substring(i-1,i))){
                            codData=codData+c.getString(0).substring(i-1,i);
                        }else{
                            Letra=Letra+c.getString(0).substring(i-1,i);
                        }
                    }
                    codData = String.valueOf(Integer.parseInt(codData)+1);
                    int lenNumerico=codData.length();
                    int lenLetra = Letra.length();
                    int lenPrincipal = c.getString(0).length();

                    int newLen = lenLetra + lenNumerico;
                    int diferencia = lenPrincipal - newLen;

                    if(diferencia == 0){

                        codnum = Letra+codData;
                    }else{
                        String validar ="%0"+(diferencia+codData.length())+"d" ;//"%04d";
                        String format = String.format(validar,Integer.parseInt(codData));
                        codnum = Letra + format;
                        //String str5 = String.format("|%010d|", 101);
                    }

                }
                bd.close();
                openHelper.close();
            } else {
//                codData = String.format("%04d", intCodData);
//                codData = String.format("%"+String.valueOf(4)+"d", intCodData);
                bd.close();
                openHelper.close();
            }
        }
        return codnum;
    }

    public boolean ValidarCodigo(int ident, String codigo) {
        boolean validar = false;
        OpenHelperSQLite openHelper = new OpenHelperSQLite(context);
        SQLiteDatabase bd = openHelper.getWritableDatabase();
        if (bd != null) {
            nombreTabla(ident);
            Cursor c;
            c = bd.rawQuery("select codigo from '" + NOMBRE_TABLA + "' WHERE codigo = " + codigo + "", null);
            if (!c.moveToFirst()) {
                validar = true;

            }
            bd.close();
            openHelper.close();
        }
        bd.close();
        openHelper.close();
        return validar;
    }

    public boolean ValidarDescripcion(int ident, String descripcion) {
        boolean validar = false;
        String desc=descripcion;
        OpenHelperSQLite openHelper = new OpenHelperSQLite(context);
        SQLiteDatabase bd = openHelper.getWritableDatabase();
        if (bd != null) {
            nombreTabla(ident);
            Cursor c;
            c = bd.rawQuery("SELECT codigo FROM '"+NOMBRE_TABLA+"' WHERE descripcion = '"+descripcion+"'",null);
            if (!c.moveToFirst()) {
                validar = true;
            }
            bd.close();
            openHelper.close();
        }
        bd.close();
        openHelper.close();
        return validar;
    }

    public void nombreTabla(int ident) {
        switch (ident) {
            case 1:
                NOMBRE_TABLA = "Local";
                break;
            case 2:
                NOMBRE_TABLA = "Piso";
            case 3:
                NOMBRE_TABLA = "Zona";
                break;
            case 4:
                NOMBRE_TABLA = "Area";
                break;
            case 5:
                NOMBRE_TABLA = "Usuario";
                break;
            case 6:
                NOMBRE_TABLA = "CentroCosto";
                break;
        }

    }

    public boolean isNumeric(String numeric){
        boolean respuesta=false;
        try{
            int n=Integer.parseInt(numeric);
            respuesta=true;

        }catch(Exception ex){
            respuesta=false;
        }

        return respuesta;
    }
}
