package com.app.tecflex.pwc_inventario.Home.Fragments.Inventario.Presenter;

import android.content.Context;

import com.app.tecflex.pwc_inventario.Home.Fragments.Inventario.Interactor.InventarioInteractor;
import com.app.tecflex.pwc_inventario.Home.Fragments.Inventario.Interfaces.InventarioInterfaces;

import java.util.ArrayList;

public class InventarioPresenter implements InventarioInterfaces.Presenter {

    private InventarioInterfaces.Vista view;
    private InventarioInterfaces.Interactor interactor;

    public InventarioPresenter(InventarioInterfaces.Vista view) {
        this.view = view;
        interactor = new InventarioInteractor(this);
    }


    @Override
    public void consultaListadoPresenter(Context context, int ident) {
        interactor.consultaListadoDataInteractor(context, ident);
    }

    @Override
    public void mostrarListadoDataPresenter(ArrayList<String> listaLocal, int ident) {
        view.mostrarListadoDataVista(listaLocal, ident);
    }

    @Override
    public void mostrarPisoListadoPresenter(ArrayList<String> listadoPiso) {
        view.mostrarPisoListadoVista(listadoPiso);
    }

    @Override
    public void consultaPisoListadoPresenter(Context context, String cod) {
        interactor.consultaPisoListadoInteractor(context, cod);
    }


}
