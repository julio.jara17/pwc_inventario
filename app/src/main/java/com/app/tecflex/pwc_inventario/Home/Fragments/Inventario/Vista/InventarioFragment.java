package com.app.tecflex.pwc_inventario.Home.Fragments.Inventario.Vista;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.tecflex.pwc_inventario.Home.Fragments.Inventario.Interfaces.InventarioInterfaces;
import com.app.tecflex.pwc_inventario.Home.Fragments.Inventario.Presenter.InventarioPresenter;
import com.app.tecflex.pwc_inventario.R;
import com.app.tecflex.pwc_inventario.Sqlite.Inventario.InventarioDAO;
import com.app.tecflex.pwc_inventario.databinding.FragmentInventarioBinding;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import java.util.ArrayList;


public class InventarioFragment extends Fragment implements InventarioInterfaces.Vista {

    private FragmentInventarioBinding binding;
    private InventarioInterfaces.Presenter presenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentInventarioBinding.inflate(inflater, container, false);
        presenter = new InventarioPresenter(this);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //aqui va todo la logica
        binding.btnBuscarLocal.setOnClickListener(v -> consultaLocalVista(getActivity(), 1));
        binding.btnBuscarZona.setOnClickListener(v -> consultaLocalVista(getActivity(), 3));
        binding.btnBuscarArea.setOnClickListener(v -> consultaLocalVista(getActivity(), 4));
        binding.btnBuscarCentroCosto.setOnClickListener(v -> consultaLocalVista(getActivity(), 5));
        binding.btnBuscarUsuario.setOnClickListener(v -> consultaLocalVista(getActivity(), 6));

    }

    @Override
    public void consultaLocalVista(Context context, int ident) {
        presenter.consultaLocalPresenter(context, ident);
    }

    @Override
    public void mostrarListaLocalVista(ArrayList<String> ListaData, int ident) {
        if (ListaData == null) {
            MensajeDialog("Login", "Se presentó un error, intentar nuevamente.");
            return;
        }
        mostrarListaData(ListaData, ident);
    }


    @Override
    public void MensajeDialog(String Title, String Mensaje) {
        new MaterialAlertDialogBuilder(getContext(), R.style.materialAlertCustom)
                .setTitle(Title)
                .setMessage(Mensaje)
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    @Override
    public void mostrarListaData(ArrayList<String> listaData, int ident) {
        final Dialog dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.lisview_item);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);

        final ListView lvLocal = dialog.findViewById(R.id.lvLocal);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, listaData);
        lvLocal.setAdapter(adapter);

        final SearchView svLocal = dialog.findViewById(R.id.svLocal);
        final TextView tvCancelar = dialog.findViewById(R.id.tvCancelaCard);
        final TextView tvAgregar = dialog.findViewById(R.id.tvAgregar);
        RelativeLayout rlFiltroList = dialog.findViewById(R.id.rlFiltroList);
        RelativeLayout rlAgregar = dialog.findViewById(R.id.rlAgregar);
        TextView tvAceptar = dialog.findViewById(R.id.tvAceptar);
        EditText tieCodAgregar = dialog.findViewById(R.id.tieCodAgregar);
        EditText tieDescAgregar = dialog.findViewById(R.id.tieDescAgregar);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();


        tvAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                rlFiltroList.setVisibility(View.INVISIBLE);
                tvAgregar.setVisibility(View.INVISIBLE);
                rlAgregar.setVisibility(View.VISIBLE);
                tvAceptar.setVisibility(View.VISIBLE);

                tvAceptar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (tieCodAgregar.getText().toString().contains(" ")) {
                            Toast.makeText(getActivity(), "Incorrecto,ingresar sin espaciado", Toast.LENGTH_SHORT).show();
                        } else {

                            InventarioDAO invent = new InventarioDAO(getActivity());
                            invent.AgregarData(tieCodAgregar.getText().toString(), tieDescAgregar.getText().toString(), ident);
                            ArrayList<String> listaData = invent.ListadoData(ident);
                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, listaData);
                            lvLocal.setAdapter(adapter);

                            rlFiltroList.setVisibility(View.VISIBLE);
                            tvAgregar.setVisibility(View.VISIBLE);
                            rlAgregar.setVisibility(View.INVISIBLE);
                            tvAceptar.setVisibility(View.INVISIBLE);
                        }
                    }
                });

            }
        });

        svLocal.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return false;
            }
        });

        tvCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rlFiltroList.getVisibility() == View.VISIBLE) {
                    dialog.dismiss();
                } else {
                    rlFiltroList.setVisibility(View.VISIBLE);
                    tvAgregar.setVisibility(View.VISIBLE);
                    rlAgregar.setVisibility(View.INVISIBLE);
                    tvAceptar.setVisibility(View.INVISIBLE);
                }

            }
        });
        lvLocal.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Object object = parent.getAdapter().getItem(position);
                String[] strListaData = object.toString().split("-");
                switch (ident) {
                    case 1:
                        binding.tvLocal.setText(strListaData[0] + " - " + strListaData[1]);
                        break;
                    case 2:
                        break;
                    case 3:
                        binding.tvZona.setText(strListaData[0] + " - " + strListaData[1]);
                        break;
                    case 4:
                        binding.tvArea.setText(strListaData[0] + " - " + strListaData[1]);
                        break;
                    case 5:
                        binding.tvCentroCosto.setText(strListaData[0] + " - " + strListaData[1]);
                        break;
                    case 6:
                        binding.tvUsuario.setText(strListaData[0] + " - " + strListaData[1]);
                        break;

                }
                dialog.dismiss();
            }
        });

    }
}