package com.app.tecflex.pwc_inventario.Login.Vista;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.fragment.app.FragmentManager;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.tecflex.pwc_inventario.BuildConfig;
import com.app.tecflex.pwc_inventario.Home.Vista.HomeActivity;
import com.app.tecflex.pwc_inventario.Login.Fragment.DialogListUserFragment;
import com.app.tecflex.pwc_inventario.Login.Interfaces.LoginInterfaces;
import com.app.tecflex.pwc_inventario.Login.Presenter.LoginPresenter;
import com.app.tecflex.pwc_inventario.R;
import com.app.tecflex.pwc_inventario.Sqlite.Login.Emtidad.UserLoginEntity;
import com.app.tecflex.pwc_inventario.Utilitarios.Global;
import com.app.tecflex.pwc_inventario.databinding.ActivityMainBinding;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.Objects;

public class MainActivity extends AppCompatActivity implements LoginInterfaces.Vista {

    private ActivityMainBinding binding;
    private LoginInterfaces.Presenter presenter;

    private static final int REQUEST_PERMISSION = 123;
    private static final int PERMISSION_COUNT = 5;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(binding.getRoot());

        presenter = new LoginPresenter(this);

        binding.tvVersion.setText(Global.versionApp);

        binding.btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validarUsuarioVista(binding.tieUsuario.getText().toString().trim(), binding.tiePassword.getText().toString().trim(), MainActivity.this);
            }
        });
        binding.tbnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MensajeSalir("Salir", "¿Seguro que desea salir de la aplicación?");
            }
        });
        binding.tvSelectUser.setText(Html.fromHtml(getResources().getString(R.string.seleccionar_usuario)));

        binding.tvSelectUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                consultaListaUsuariosVista(MainActivity.this);
            }
        });

        binding.tiePassword.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    validarUsuarioVista(binding.tieUsuario.getText().toString().trim(), binding.tiePassword.getText().toString().trim(), MainActivity.this);
                }
                return false;
            }
        });

    }

    @Override
    public void validarUsuarioVista(String user, String password, Context context) {
        presenter.validarUsuarioPresenter(user, password, context);
    }

    @Override
    public void mostrarRespuestaLoginVista(ArrayList<UserLoginEntity> respuestaLogin) {
        if (respuestaLogin != null) {
            ocultarTeclado(this);
            Global.codInventariador=respuestaLogin.get(0).getCodigo();
            Global.descInventariador=respuestaLogin.get(0).getDescripcion();
            Intent intent = new Intent(MainActivity.this, HomeActivity.class);
            startActivity(intent);
            finish();
        } else {
            ocultarTeclado(this);
            Snackbar.make(binding.idContenedorPrincipal, "Datos incorrectos", Snackbar.LENGTH_SHORT).setBackgroundTint(getResources().getColor(android.R.color.black)).show();
            //MensajeDialog("Error Login", "Datos incorrectos");
        }
    }

    @Override
    public void consultaListaUsuariosVista(Context context) {
        presenter.consultaListaUsuariosPresenter(context);
    }

    @Override
    public void mostrarListaUsuariosVista(ArrayList<String> usuarios) {

        if(usuarios ==null){
            MensajeDialog("Login","Se presentó un error, intentar nuevamente.");
            return;
        }
        Global.listaUsuarios = usuarios;

        FragmentManager fm = getSupportFragmentManager();
        DialogListUserFragment dialog = new DialogListUserFragment();
        dialog.show(fm,"fragment_dialog_list_user");


    }

    @Override
    public void MensajeSalir(String Title, String Mensaje) {

        new MaterialAlertDialogBuilder(MainActivity.this, R.style.materialAlertCustom)
                .setTitle(Title)
                .setMessage(Mensaje)
                .setCancelable(false)
                .setPositiveButton("Sí", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent cerrar = new Intent(Intent.ACTION_MAIN);
                        cerrar.addCategory(Intent.CATEGORY_HOME);
                        cerrar.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(cerrar);
                        finish();
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    @Override
    public void MensajeDialog(String Title, String Mensaje) {
        new MaterialAlertDialogBuilder(MainActivity.this, R.style.materialAlertCustom)
                .setTitle(Title)
                .setMessage(Mensaje)
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    @Override
    public void mostrarTeclado(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    @Override
    public void ocultarTeclado(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        assert imm != null;
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

    }

    @Override
    public void settearDescUsuario(String codigo) {
        binding.tieUsuario.setText(codigo);
        binding.tiePassword.requestFocus();
        mostrarTeclado(MainActivity.this);
    }

    //Permisos
    private static final String[] PERMISSIONS = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_WIFI_STATE,
            Manifest.permission.ACCESS_NETWORK_STATE
    };

    private boolean arePermissionDenied() {
        int p = 0;
        while (p < PERMISSION_COUNT) {
            if (checkSelfPermission(PERMISSIONS[p]) != PackageManager.PERMISSION_GRANTED) {
                return true;
            }
            p++;
        }
        return false;
    }
    @Override
    protected void onResume() {
        super.onResume();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (arePermissionDenied()) {
                requestPermissions(PERMISSIONS, REQUEST_PERMISSION);
                return;
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PERMISSION && grantResults.length > 0) {
            if (arePermissionDenied()) {
                ((ActivityManager) Objects.requireNonNull(this.getSystemService(ACTIVITY_SERVICE))).clearApplicationUserData();
                recreate();
            }else{

                try {
                    //guardar(String.valueOf(Build.getSerial()));
                }catch (Exception e){
                    //guardar("TECFLEX28092020CBX");
                }
                //txt.setText(Build.getSerial());
            }
        }
    }
}